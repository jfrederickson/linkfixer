import sys, os, argparse, hashlib

#links = {}
DESTDIR="/home/jfred/sources/linkfixer/dest"

def getfiles(iterdir):
    ret = []
    for root, dirs, files in os.walk(iterdir):
        for name in files:
            ret.append(os.path.join(root, name))
    return ret

def addlinks(iterdir, listkey, links={}, checkcoll=False):
    for file in getfiles(iterdir):
        print("Hashing {}".format(file))
        with open(file) as f:
            buf = f.read(2048)
            hash = hashlib.md5(buf).hexdigest()
            if checkcoll:
                if hash in links.keys():
                    print("Collision detected! {} = {}".format(file, links[hash]))
                    sys.exit(1)
            if hash not in links.keys():
                links[hash] = [None] * 2
            links[hash][listkey] = file
    return links

def buildlist(dir1, dir2):
    links = addlinks(dir1, 0, checkcoll=True)
    links = addlinks(dir2, 1, links)
    return links

def createlinks(linklist, workdir):
    os.chdir(workdir)
    for link in linklist.values():
        if link[0] and link[1]:
            print("Link! {} -> {}".format(link[0], link[1]))
            if not os.path.exists(link[1]):
                os.link(link[0], link[1])
            else:
                print("Whoa there! This destination file already exists! {}".format(link[1]))

def main():
    parser = argparse.ArgumentParser(description="Rebuild hardlinks in a new directory tree")

    parser.add_argument("source_sorted", help="Source directory for sorted files")
    parser.add_argument("source_unsorted", help="Source directory for unsorted files")
    parser.add_argument("dest_root")

    args = parser.parse_args()

    linklist = buildlist(args.source_sorted, args.source_unsorted)
    createlinks(linklist, args.dest_root)

if __name__ == "__main__":
    main()

