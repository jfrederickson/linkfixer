import os, argparse

#links = {}
DESTDIR="/home/jfred/sources/linkfixer/dest"

def getfiles(iterdir):
    ret = []
    for root, dirs, files in os.walk(iterdir):
        for name in files:
            ret.append(os.path.join(root, name))
    return ret

def addlinks(iterdir, listkey, links={}):
    for file in getfiles(iterdir):
        f = os.stat(file)
        if f.st_ino not in links.keys():
            links[f.st_ino] = [None] * 2
        links[f.st_ino][listkey] = file
    return links

def buildlist(dir1, dir2):
    links = addlinks(dir1, 0)
    links = addlinks(dir2, 1, links)
    return links

def createlinks(linklist, workdir):
    os.chdir(workdir)
    for link in linklist.values():
        if link[0] and link[1]:
            print("Link! {} -> {}".format(link[0], link[1]))
            #os.link(link[0], link[1])

def main():
    parser = argparse.ArgumentParser(description="Rebuild hardlinks in a new directory tree")

    parser.add_argument("source_sorted", help="Source directory for sorted files")
    parser.add_argument("source_unsorted", help="Source directory for unsorted files")
    parser.add_argument("dest_root")

    args = parser.parse_args()

    linklist = buildlist(args.source_sorted, args.source_unsorted)
    createlinks(linklist, args.dest_root)

if __name__ == "__main__":
    main()

